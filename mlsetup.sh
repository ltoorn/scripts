#!/bin/sh

if [ $# == 0 ]
then
  echo "Please enter a project name."
  exit
fi

echo "---"
echo "Setting up ML project $1."
echo "---"

build0="if [ ! -d target ]\nthen\n\tmkdir target\nfi\n\n"
build1="if [ \$# == 0 ]\nthen\n\techo -e \"Options:\nr\t-\trelease build;\nd\t-\tdebug build;\nclean\t-\tclean out target.\"\n"
build2="elif [ \$1 == \"d\" ]\nthen\n\tpushd src\nocamlopt -g -o ../target/$1-debug main.ml\npopd\n"
build3="elif [ \$1 == \"r\" ]\nthen\n\tpushd src\nocamlopt -O2 -o ../target/$1 main.ml\npopd\n"
build4="elif [ \$1 == \"clean\" ]\nthen\n\tpushd src\n rm *.cmi *.cmx *.o\npopd\nrm -r target\n"
build5="else\n\techo \"Ignoring unknown option: \$1\"\nfi"

main="\nlet hello =\n  print_endline \"Hello!\"\n;;\n\nhello\n"

mkdir resources
mkdir src
mkdir target

touch README.adoc

cp ~/dev/copyright/gpl/LICENSE .

echo -e "$1" > .gitignore
echo -e "target/" >> .gitignore
echo -e "*.cmi\n*.cmx\n*.o\n" >> .gitignore

cat ~/dev/copyright/gpl/notice.ml > src/main.ml

echo -e $main >> src/main.ml

echo -e $build0 > build.sh
echo -e $build1 >> build.sh
echo -e $build2 >> build.sh
echo -e $build3 >> build.sh
echo -e $build4 >> build.sh
echo -e $build5 >> build.sh

chmod +x build.sh

git init
git add --all
git commit -am "Init."

echo "---"
echo "Done!"
echo "---"

