#!/bin/sh

if [ $# == 0 ]
then
  echo "Please enter a project name."
  exit
fi

build0="if [ ! -d target ]\nthen\n\tmkdir target\nfi\n\n"
build1="if [ \$# == 0 ]\nthen\n\techo -e \"Options:\nr\t-\trelease build;\nd\t-\tdebug build;\nclean\t-\tclean out target.\"\n"
build2="elif [ \$1 == \"d\" ]\nthen\n\tclang -g -std=c99 -pedantic-errors -Wall -DDEBUG=1 -o target/$1-debug src/main.c\n"
build3="elif [ \$1 == \"r\" ]\nthen\n\tclang -O1 -std=c99 -o target/$1 src/main.c\n"
build4="elif [ \$1 == \"clean\" ]\nthen\n\trm target/*\n"
build5="else\n\techo \"Ignoring unknown option: \$1\"\nfi"

echo -e $build0 > build.sh
echo -e $build1 >> build.sh
echo -e $build2 >> build.sh
echo -e $build3 >> build.sh
echo -e $build4 >> build.sh
echo -e $build5 >> build.sh

chmod +x build.sh

echo "---"
echo "Generated a build script for project $1."
echo "---"

