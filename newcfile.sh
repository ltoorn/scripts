#!/bin/sh

if [ $# != 2 ]
then
	echo "Please enter the target directory and file name."
	exit
fi

pushd $1

cat ~/dev/copyright/mit/notice.c >> $2

popd
