#!/bin/sh

if [ $# == 0 ]
then
  echo "Please enter a project name."
  exit
fi

echo "---"
echo "Setting up Go project $1."
echo "---"

mkdir $1
pushd $1

build0="if [ ! -d target ]\nthen\n\tmkdir target\nfi\n\n"
build2="if [ \$# == 0 ]\nthen\n\tgo build -o target/$1 src/main/main.go\n"
build3="elif [ \$1 == \"clean\" ]\nthen\n\trm target/*\ngo clean\n"
build4="else\n\techo \"Ignoring unknown option: \$1\"\nfi"

main="package main\n\nimport \"fmt\"\n\nfunc main() {\n\tfmt.Println(\"Hello, World!\")\n}\n"

mkdir resources
mkdir -p src/main

touch README.adoc

cp ~/dev/copyright/mit/LICENSE .

echo -e "$1" > .gitignore
echo -e "target/" >> .gitignore

cat ~/dev/copyright/mit/notice.go > src/main/main.go
echo -e $main >> src/main/main.go

echo -e $build0 > build.sh
echo -e $build1 >> build.sh
echo -e $build2 >> build.sh
echo -e $build3 >> build.sh
echo -e $build4 >> build.sh

chmod +x build.sh

git init
git add --all
git commit -am "Init."

popd

echo "---"
echo "Done!"
echo "---"

