#!/bin/sh

echo "---"
echo "Setting up Nim project $1."
echo "---"

copyright="# Copyright © 2019 Lucas Christiaan van den Toorn\n# Distribution of this source code is governed by the MIT license (see LICENSE)\n\n"

build="nim c -o:./target/$1-debug ./src/main.nim"

mkdir src
mkdir target
mkdir resources

touch README.adoc
cp ~/dev/copyright/mit/LICENSE .

echo -e "/target/" > .gitignore

echo -e $copyright > src/main.nim

echo -e $copyright > src/$1.nim

echo -e $build > build.sh

chmod +x build.sh

git init
git add --all
git commit -am "Init."

echo "---"
echo "done"
echo "---"
