#!/bin/sh

if [ $# == 0 ]
then
  echo "Please enter a project name."
  exit
fi

echo "---"
echo "Setting up C project $1."
echo "---"

mkdir $1
pushd $1

main="#include <stdio.h>\n\nint\nmain(int argc, char **argv)\n{\n\tprintf(\"Hello, World!\\\n\");\n\n\treturn 0;\n}\n"

mkdir resources
mkdir src
mkdir target

touch README.adoc

cp ~/dev/copyright/mit/LICENSE .

echo -e "$1" > .gitignore
echo -e "target/" >> .gitignore

cat ~/dev/copyright/mit/notice.c > src/main.c
echo -e $main >> src/main.c

cbuildscript.sh $1

git init
git add --all
git commit -am "Init."

popd

add_to_bashrc.sh "alias cd$1=\"cd ~/dev/c/$1\""

echo "---"
echo "Done!"
echo "---"

