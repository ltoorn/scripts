#!/bin/sh

# This script assumes the Clojure project has already been setup using Leiningen,
# and should be run from the root directory of that project.

if [ $# == 0 ]
then
    echo "Please enter the project name."
    exit
fi

rm README.md

echo "TODO(ltoorn): Write documentation." > README.adoc
cp ~/dev/copyright/epl/LICENSE .

cat ~/dev/copyright/epl/notice.clj > ./src/$1/tmp.clj
cat ./src/$1/core.clj >> ./src/$1/tmp.clj
cat ./src/$1/tmp.clj > ./src/$1/core.clj
rm ./src/$1/tmp.clj

git init
git add --all
git commit -am "Init"

